#!/bin/bash
################################################################################
# Copyright 2019 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

set -e

# ROS Calibration DFS Files
ROS_CAL_PATH=/tmp
ROS_CAL_TAR=$ROS_CAL_PATH/calibrationdata.tar.gz
CAL_DIR=$ROS_CAL_PATH/calibrationdata
CAL_FILE=$CAL_DIR/ost.yaml
SNAV_FILE=$CAL_DIR/calibration.downward.xml
PY_TOOLS_PATH=py_tools

OPENCV_INTRINSICS_FILE=$CAL_DIR/opencv_intrinsics.yml


# extract calibration data in /tmp/ from the TAR ROS created
mkdir -p $CAL_DIR
tar -xzf $ROS_CAL_TAR --directory $CAL_DIR

# copy sample file over, this will be updated with the new values in the next step
cp -f sample_cal_files/calibration.downward.xml $SNAV_FILE

# generate snav compatibile calibration file
python $PY_TOOLS_PATH/downwardcalibfilegen.py -y $CAL_FILE -x $SNAV_FILE

# load file to target
adb shell mkdir -p /etc/snav/
adb push $SNAV_FILE /etc/snav/.
adb shell mkdir -p /home/root/.ros/camera_info/
adb push $CAL_FILE /home/root/.ros/camera_info/downward.yaml

# new files for MPA just use standard opencv cal files
adb shell mkdir -p /data/modalai/
adb push $OPENCV_INTRINSICS_FILE /data/modalai/opencv_tracking_intrinsics.yml

adb shell sync

echo "DONE"

