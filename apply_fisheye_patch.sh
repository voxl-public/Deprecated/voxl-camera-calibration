#!/bin/bash

set -e

PATCHED1=patched_files/calibrator.py
INDIGO_FILE1=/opt/ros/indigo/lib/python2.7/dist-packages/camera_calibration/calibrator.py
MELODIC_FILE1=/opt/ros/melodic/lib/python2.7/dist-packages/camera_calibration/calibrator.py

PATCHED2=patched_files/camera_calibrator.py
INDIGO_FILE2=/opt/ros/indigo/lib/python2.7/dist-packages/camera_calibration/camera_calibrator.py
MELODIC_FILE2=/opt/ros/melodic/lib/python2.7/dist-packages/camera_calibration/camera_calibrator.py

PATCHED3=patched_files/cameracalibrator.py
INDIGO_FILE3=/opt/ros/indigo/lib/camera_calibration/cameracalibrator.py
MELODIC_FILE3=/opt/ros/melodic/lib/camera_calibration/cameracalibrator.py

BACKUP=true

if [ -f $INDIGO_FILE1 ]; then
    MODE=INDIGO
    FILE1=$INDIGO_FILE1
    FILE2=$INDIGO_FILE2
    FILE3=$INDIGO_FILE3
    sudo apt install ros-indigo-opencv3
elif [ -f  $MELODIC_FILE1 ]; then
    MODE=MELODIC
    FILE1=$MELODIC_FILE1
    FILE2=$MELODIC_FILE2
    FILE3=$MELODIC_FILE3
else
    echo "Patch only tested in ROS Indigo and Melodic"
    echo "exiting"
    exit 1
fi


# check for backup files
if ! [ -f ${FILE1}.bak ]; then
    echo "backing up file 1"
    sudo cp $FILE1 ${FILE1}.bak
fi
if ! [ -f ${FILE2}.bak ]; then
    echo "backing up file 2"
    sudo cp $FILE2 ${FILE2}.bak
fi
if ! [ -f ${FILE2}.bak ]; then
    echo "backing up file 2"
    sudo cp $FILE2 ${FILE2}.bak
fi


# copy over new files
sudo cp $PATCHED1 $FILE1
sudo cp $PATCHED2 $FILE2
sudo cp $PATCHED3 $FILE3

echo DONE