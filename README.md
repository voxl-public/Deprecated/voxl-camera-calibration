# voxl-camera-calibration

This project contains the host-pc side tools for calibrating the downward facing tracking camera (for VIO) and stereo camera pair.

For instructions on how to use, follow the public documentation here:

https://docs.modalai.com/calibrate-cameras/