#!/usr/bin/env python
import rospy
import tf
import geometry_msgs.msg
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion, quaternion_from_euler



quaternion = tf.transformations.quaternion_from_euler(1.343903524, 0.0, -1.57079632679)
#type(pose) = geometry_msgs.msg.Pose

pose = geometry_msgs.msg.Pose()
pose.orientation.x = quaternion[0]
pose.orientation.y = quaternion[1]
pose.orientation.z = quaternion[2]
pose.orientation.w = quaternion[3]

print( str(pose.orientation))

