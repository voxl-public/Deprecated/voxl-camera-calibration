#!/bin/bash

set -e

INDIGO_FILE1=/opt/ros/indigo/lib/python2.7/dist-packages/camera_calibration/calibrator.py
MELODIC_FILE1=/opt/ros/melodic/lib/python2.7/dist-packages/camera_calibration/calibrator.py

INDIGO_FILE2=/opt/ros/indigo/lib/python2.7/dist-packages/camera_calibration/camera_calibrator.py
MELODIC_FILE2=/opt/ros/melodic/lib/python2.7/dist-packages/camera_calibration/camera_calibrator.py

INDIGO_FILE3=/opt/ros/indigo/lib/camera_calibration/cameracalibrator.py
MELODIC_FILE3=/opt/ros/melodic/lib/camera_calibration/cameracalibrator.py



if [ -f $INDIGO_FILE1 ]; then
    FILE1=$INDIGO_FILE1
    FILE2=$INDIGO_FILE2
    FILE3=$INDIGO_FILE3
    sudo apt install ros-indigo-opencv3
elif [ -f  $MELODIC_FILE1 ]; then
    FILE1=$MELODIC_FILE1
    FILE2=$MELODIC_FILE2
    FILE3=$MELODIC_FILE3
else
    echo "Patch only tested in ROS Indigo and Melodic"
    echo "exiting"
    FILE1=$MELODIC_FILE1
    FILE2=$MELODIC_FILE2
    FILE3=$MELODIC_FILE3
    #exit 1
fi


if [ -f ${FILE1}.bak ]; then
    sudo mv -f ${FILE1}.bak ${FILE1}
else
    echo "backup file doesn't exist"
    echo "file: ${FILE1}.bak"
    echo "Probably file was never patched or has already been unpatched"
fi

if [ -f ${FILE2}.bak ]; then
    sudo mv -f ${FILE2}.bak ${FILE2}
else
    echo "backup file doesn't exist"
    echo "file: ${FILE2}.bak"
    echo "Probably file was never patched or has already been unpatched"
fi

if [ -f ${FILE3}.bak ]; then
    sudo mv -f ${FILE3}.bak ${FILE3}
else
    echo "backup file doesn't exist"
    echo "file: ${FILE3}.bak"
    echo "Probably file was never patched or has already been unpatched"
fi

echo "DONE"

